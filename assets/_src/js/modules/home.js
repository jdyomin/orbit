var oApp = oApp || {};

oApp.Main = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.main'),
                stop: false,
                ciclingStop: false
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this,
                characters = '.characters',
                character = $('.js-character');

            if ($(window).width() > 1024) {
                character.find('svg').on({
                    'mouseenter': function () {
                        $(this).parents('.js-character').addClass('_hover');
                    },
                    'mouseleave': function () {
                        $(this).parents('.js-character').removeClass('_hover');
                    }
                });

                character.find('svg path').on({
                    'mouseenter': function () {
                        $(this).parents('.js-character').addClass('_hover-cont');
                    },
                    'mouseleave': function () {
                        $(this).parents('.js-character').removeClass('_hover-cont');
                    }
                });
            } else {
                character.find('svg').on('click', function () {
                    if ($(this).parents('.js-character').is('._hover')) return false;

                    $('.js-character').removeClass('_hover');
                    $(this).parents('.js-character').addClass('_hover');
                });
            }

            var stop = false;

            oApp.$W.on('resize', function () {
                if (!stop) {
                    stop = true;

                    setTimeout(function () {
                        _this._restartAnimation();
                        stop = false;
                    }, 250);
                }
            });
        },
        _render: function () {
            var _this = this;

            initSlider();

            if (oApp.$W.width() > 1024) {
                _this._cycling();

                oApp.$D.one('page-load', function () {
                    if (!_this.tl) {
                        _this._startAnimation();
                    } else {
                        _this._restartAnimation();
                    }
                });
            } else {
                _this.swiper.slideTo(2);
            }

            function initSlider() {
                var options;

                options = {
                    slidesPerView: 4,
                    spaceBetween: 0,
                    freeMode: true,
                    simulateTouch: false,
                    loopAdditionalSlides: 6,
                    loop: true,
                    observer: true,
                    breakpoints: {
                        1024: {
                            slidesPerView: 1.5
                        }
                    }
                };

                _this.swiper = new Swiper('.characters', options);
            }

            this.first = this.swiper.translate;

            if ($(window).width() > 1024) {
                this._revealInit();
            } else {
                var revealers = $('#rev1, #rev2, #rev3, #rev4');
                revealers.removeAttr('id');
            }
        },
        _restartAnimation: function () {
            var _this = this,
                characters = '.characters';

            _this.tl.pause(0);
            _this.tl.clear();
            $('.intro__title, .intro__btn, .intro__subtitle._1, .intro__subtitle._2, .intro__subtitle._3, .intro__subtitle._4, .intro__subtitle._5, .intro__subtitle._6, .js-question-mark').removeInlineCss();

            if (oApp.$W.width() > 1024) {
                oApp.$D.off('mouseenter.drag', characters);
                oApp.$D.off('mouseleave.drag', characters);
                oApp.$D.off('mousemove.drag', characters);

                _this._cyclingPause();

                setTimeout(function () {
                    _this._cyclingContinue();
                    _this._startAnimation();
                }, 100);
            } else {
                _this._cyclingPause();
            }
        },
        _addRotateEvents: function () {
            var _this = this,
                characters = '.characters';

            oApp.$D.on('mouseenter.drag', characters, function(e){
                if (!_this.ciclingStop) return;

                _this.stop = false;
                _this._startRotate(e);
                $(characters).addClass('_active');
            });

            oApp.$D.on('mouseleave.drag', characters, function(e){
                setTimeout(function () {
                    _this.stop = true;
                    $(characters).removeClass('_active');
                }, 10);
            });

            oApp.$D.on('mousemove.drag', characters, function(e){
                if (!_this.ciclingStop) return;

                _this.stop = true;

                setTimeout(function () {
                    _this.stop = false;
                    _this._startRotate(e);
                }, 10);
            });
        },
        _startRotate: function (e) {
            var _this = this,
                movementStrength = 5,
                width = movementStrength / $(window).width();

            var pageX = e.pageX - ($(window).width() / 2),
                newvalueX = width * pageX * -1,
                pos = _this.swiper.getTranslate(),
                newpos = pos + newvalueX*2;

            if (_this.swiper.isEnd) {
                var leftpos = (this.first / 3),
                    last = this.first + leftpos;

                _this.swiper.setTranslate(last);
            } else if (_this.swiper.isBeginning) {
                _this.swiper.setTranslate(this.first);
            } else {
                _this.swiper.setTranslate(newpos);
                _this.swiper.update();
            }

            if (!_this.stop) {
                setTimeout(function () {
                    _this._startRotate(e);
                }, 0);
            }
        },
        _cycling: function () {
            var _this = this,
                movementStrength = 2,
                width = movementStrength / $(window).width(),
                pageX = 400 - ($(window).width() / 2),
                newvalueX = width * pageX;

            var pos = _this.swiper.getTranslate(),
                newpos = pos + newvalueX*2;

            if (_this.swiper.isEnd) {
                var leftpos = (this.first / 3),
                    last = this.first + leftpos;

                _this.swiper.setTranslate(last);
            }  else {
                _this.swiper.setTranslate(newpos);
                _this.swiper.update();
            }

            if (!_this.ciclingStop) {
                setTimeout(function () {
                    _this._cycling();
                }, 10);
            }
        },
        _cyclingContinue: function () {
            this.ciclingStop = false;
            this.stop = true;
            $('.characters').addClass('_cycling');
            this._cycling();
        },
        _cyclingPause: function () {
            this.ciclingStop = true;
            $('.characters').removeClass('_cycling');
        },
        _cyclingMobileContinue: function () {
            this.ciclingStop = false;
            this.stop = true;

            $('.characters').addClass('_cycling');
            $('.js-character').removeClass('_hover');
        },
        _cyclingMobilePause: function () {
            this.ciclingStop = true;
            $('.characters').removeClass('_cycling');
        },
        _startAnimation: function () {
            var _this = this,
                tl = new TimelineMax(),
                introTitle = '.intro__title',
                introBtn = '.intro__btn',
                introBtnH = $(introBtn).height(),
                title1 = '.intro__subtitle._1',
                title1H = $(title1).height(),
                title1Tr = '-' + (title1H + 15),
                title2 = '.intro__subtitle._2',
                title2H = $(title2).height(),
                title2Tr = '-' + (title2H + 15),
                title3 = '.intro__subtitle._3',
                title3H = $(title3).height(),
                title3Tr = '-' + (title3H + 15),
                title4 = '.intro__subtitle._4',
                title4H = $(title4).height(),
                title4Tr = '-' + (title4H + 15),
                title5 = '.intro__subtitle._5',
                title5H = $(title5).height(),
                title5Tr = '-' + (title5H + 15),
                title6 = '.intro__subtitle._6',
                title6H = $(title6).height(),
                title6Tr = '-' + (title6H + 15),
                delay = 3,
                questionMark = $('.js-question-mark');

            this.tl = tl;

            tl.fromTo(introTitle, 2, {css:{'top': '-50px', 'opacity': 0}}, {css:{'top': 0, 'opacity': 1}});
            tl.fromTo(introBtn, 1, {css:{'bottom': '12rem', 'opacity': 0, 'max-height': 0}}, {css:{'bottom': '16.8rem', 'max-height': introBtnH, 'opacity': 1}});
            tl.fromTo(title1, 1, {css:{'y': title1Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title1H}});
            tl.to(title1, 1, {css:{'y': title1Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}, delay: delay});
            tl.fromTo(title2, 1, {css:{'y': title2Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title2H}});
            tl.to(title2, 1, {css:{'y': title2Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}, delay: delay});
            tl.fromTo(title3, 1, {css:{'y': title3Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title3H}});
            tl.to(title3, 1, {css:{'y': title3Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}, delay: delay});
            tl.fromTo(title4, 1, {css:{'y': title4Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title4H}, onStart: function () {
                questionMark.addClass('_active');
                _this._cyclingPause();
            }});
            tl.to(title4, 1, {css:{'y': title4Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}, delay: delay});
            tl.fromTo(title5, 1, {css:{'y': title5Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title5H}});
            tl.to(title5, 0.5, {css:{'y': title5Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}, delay: delay});
            tl.fromTo(title6, 0.5, {css:{'y': title6Tr, 'x': '-50%', 'opacity': 0, 'max-height': 0}}, {css:{'y': 0, 'x': '-50%', 'opacity': 1, 'max-height': title6H}, onComplete:function () {
                questionMark.removeClass('_active');
                _this._addRotateEvents();
                setTimeout(function () {
                    questionMark.hide();
                }, 300);
                animFinished();
            }});
        },
        _revealInit: function () {
            var scrollElemToWatch_1 = document.getElementById('rev1'),
                watcher_1 = scrollMonitor.create(scrollElemToWatch_1, -300),
                rev1 = new RevealFx(scrollElemToWatch_1, {
                    revealSettings : {
                        bgcolor: '#114573',
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev1').className="block-revealer numbered";
                        }
                    }
                }),
                rev2 = new RevealFx(document.querySelector('#rev2'), {
                    revealSettings : {
                        bgcolor: '#114573',
                        delay: 250,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev2').className="block-revealer numbered"
                        }
                    }
                }),
                rev3 = new RevealFx(document.querySelector('#rev3'), {
                    revealSettings : {
                        bgcolor: '#114573',
                        delay: 500,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev3').className="block-revealer numbered"
                        }
                    }
                }),
                rev4 = new RevealFx(document.querySelector('#rev4'), {
                    revealSettings : {
                        bgcolor: '#114573',
                        delay: 500,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content video-content';
                        }
                    }
                });

            watcher_1.enterViewport(function() {
                rev1.reveal();
                rev2.reveal();
                rev3.reveal();
                rev4.reveal();
                watcher_1.destroy();
            });
        }
    };
}());
