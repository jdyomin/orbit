var oApp = oApp || {};

oApp.Nominees = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.nominees')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this,
                plus = $('.js-plus'),
                friends = $('.js-friends-list');

            plus.on('click', function() {
                var n = $(this).closest('.swiper-slide').index();

                _this.slider.slideTo(n,1000,false );
            });

            friends.find('input[name="friends"]').one('change', function () {
                $('.js-talent').addClass('_show');
            });
        },
        _render: function () {
            var _this = this;

            $(".scrollable").mCustomScrollbar();

            this.slider = new Swiper('.swiper-container', {
                pagination: {
                    type: 'fraction',
                    el: '.swiper-pagination',
                    renderFraction: function (currentClass, totalClass) {
                        return '<span class="' + currentClass + '"></span>' +
                            ' из ' +
                            '<span class="' + totalClass + '"></span>';
                    }
                },
                scrollbar: {
                    el: '.swiper-scrollbar',
                    draggable: true,
                    dragSize: 305,
                    snapOnRelease: true
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                initialSlide: 0,
                slidesPerView: 'auto',
                centeredSlides: true,
                slideToClickedSlide: true,
                breakpoints: {
                    1024: {
                        scrollbar:{
                            dragSize: 140
                        }
                    }
                }
            });
        }
    };
}());