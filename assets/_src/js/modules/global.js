var oApp = oApp || {};

oApp.Global = library(function() {
    return {
        init: function () {
            this.elements = {};

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this,
                menuTrigger = $('.js-menu-trigger');

            menuTrigger.on('click', function() {
                $(this).toggleClass('close');
                $('.js-menu').toggleClass('opened');
                $('body').toggleClass('overflow');
            });

            $(document).on('click', '.js-filters-trigger', function() {
                $('.filters').addClass('opened');
            });

            $(document).on('click', '.js-filters-close', function() {
                $('.filters').removeClass('opened');
            });
        },
        _render: function () {
            var _this = this;

            setTimeout(function () {
                $('body').removeClass('loading');
            }, 1000);

            svg4everybody();

            $(".menu-scrollable").mCustomScrollbar();
            $(".js-modal-wrapper").mCustomScrollbar();
        },
        runPreloader: function () {
            var preloader = $('.preloader'),
                wrap = $('.js-preloader'),
                $circle = $('#bar');

            preloader.fadeIn(function () {
                wrap.addClass('in');
                $circle.css({ strokeDashoffset: 0});
            });

            setTimeout(function () {
                preloader.fadeOut(function () {
                    oApp.$D.trigger('page-load');
                });
            }, 2000);
        }
    };
}());