var oApp = oApp || {};

oApp.Nominant = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.nominant')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this;


        },
        _render: function () {
            var _this = this;

            if ($(window).width() > 1024) {
                setTimeout(function () {
                    _this._revealInit();
                }, 600);
            } else {
                var revealers = $('#rev5, #rev6, #rev7');
                revealers.removeAttr('id');
            }
        },
        _revealInit: function () {
            var scrollElemToWatch_2 = document.getElementById('rev5'),
                watcher_2 = scrollMonitor.create(scrollElemToWatch_2, -300),
                rev5 = new RevealFx(scrollElemToWatch_2, {
                    revealSettings : {
                        bgcolor: '#114573',
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev5').className="block-revealer numbered";
                        }
                    }
                }),
                rev6 = new RevealFx(document.querySelector('#rev6'), {
                    revealSettings : {
                        bgcolor: '#114573',
                        delay: 250,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev6').className="block-revealer numbered"
                        }
                    }
                }),
                rev7 = new RevealFx(document.querySelector('#rev7'), {
                    revealSettings : {
                        bgcolor: '#114573',
                        delay: 500,
                        onCover: function(contentEl, revealerEl) {
                            contentEl.style.opacity = 1;
                            contentEl.className = 'block-revealer__content nominate-item';
                            document.getElementById('rev7').className="block-revealer numbered"
                        }
                    }
                });

            watcher_2.enterViewport(function() {
                rev5.reveal();
                rev6.reveal();
                rev7.reveal();
                watcher_2.destroy();
            });
        }
    };
}());