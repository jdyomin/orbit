var oApp = oApp || {};

oApp.NomineesList = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.nominees_list')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this;


        },
        _render: function () {
            var _this = this;

            $('.custom-select').selectric({
                multiple: {
                    separator: ', ',
                    keepMenuOpen: true,
                    maxLabelEntries: false
                }
            });

            $(".selectric-scroll").mCustomScrollbar();
        }
    };
}());