var oApp = oApp || {};

oApp.Product = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.product_page')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this;
        },
        _render: function () {
            var _this = this;

            var productSlider = new Swiper('.product-slider', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true
                },
                on: {
                    slideChange: function() {
                        var activeSlide = productSlider.activeIndex;
                        var paginationSlides = $('.product-pagination .pagination-slide');
                        $.each(paginationSlides, function() {
                            if ($(this).index() === activeSlide) {
                                $(this).trigger('click');
                                $(this).siblings().removeClass('pagination-slide-active');
                                $(this).addClass('pagination-slide-active');
                                productPagination.slideTo(activeSlide, 600);
                            }
                        })
                    }
                },
                breakpoints: {
                    1024: {
                        slidesPerView: 'auto',
                        centeredSlides: true,
                        effect: 'slide',
                        fadeEffect: {
                            crossFade: false
                        },
                        slideToClickedSlide: true
                    }
                }
            });

            var productPagination = new Swiper('.product-pagination', {
                pagination: {
                    type: 'fraction',
                    el: '.swiper-pagination',
                    renderFraction: function (currentClass, totalClass) {
                        return '<span class="' + currentClass + '"></span>' +
                            ' из ' +
                            '<span class="' + totalClass + '"></span>';
                    }
                },
                centeredSlides: true,
                slidesPerView: 'auto',
                slideToClickedSlide: true,
                scrollbar: {
                    el: '.swiper-scrollbar',
                    draggable: true,
                    dragSize: 305,
                    snapOnRelease: true
                },
                on: {
                    slideChange: function() {
                        var activeSlide = productPagination.activeIndex;

                        productSlider.slideTo(activeSlide, 600);
                    }
                }
            })
        }
    };
}());