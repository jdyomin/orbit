;( function( window ) {
    'use strict';
    /*! modernizr 3.5.0 (Custom Build) | MIT *
     * https://modernizr.com/download/?-csstransitions-prefixed !*/
    !function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,i,s,l;for(var u in g)if(g.hasOwnProperty(u)){if(e=[],n=g[u],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,i=0;i<e.length;i++)s=e[i],l=s.split("."),1===l.length?Modernizr[l[0]]=o:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=o),S.push((o?"":"no-")+l.join("-"))}}function i(e,n){return!!~(""+e).indexOf(n)}function s(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function l(e,n){return function(){return e.apply(n,arguments)}}function u(e,n,t){var o;for(var i in e)if(e[i]in n)return t===!1?e[i]:(o=n[e[i]],r(o,"function")?l(o,t||n):o);return!1}function a(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function f(n,t,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,n,t);var i=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(i){var s=i.error?"error":"log";i[s].call(i,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&n.currentStyle&&n.currentStyle[r];return o}function p(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):E?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function d(){var e=n.body;return e||(e=p(E?"svg":"body"),e.fake=!0),e}function c(e,t,r,o){var i,s,l,u,a="modernizr",f=p("div"),c=d();if(parseInt(r,10))for(;r--;)l=p("div"),l.id=o?o[r]:a+(r+1),f.appendChild(l);return i=p("style"),i.type="text/css",i.id="s"+a,(c.fake?c:f).appendChild(i),c.appendChild(f),i.styleSheet?i.styleSheet.cssText=e:i.appendChild(n.createTextNode(e)),f.id=a,c.fake&&(c.style.background="",c.style.overflow="hidden",u=z.style.overflow,z.style.overflow="hidden",z.appendChild(c)),s=t(f,e),c.fake?(c.parentNode.removeChild(c),z.style.overflow=u,z.offsetHeight):f.parentNode.removeChild(f),!!s}function m(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(a(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var i=[];o--;)i.push("("+a(n[o])+":"+r+")");return i=i.join(" or "),c("@supports ("+i+") { #modernizr { position: absolute; } }",function(e){return"absolute"==f(e,null,"position")})}return t}function y(e,n,o,l){function u(){f&&(delete b.style,delete b.modElem)}if(l=r(l,"undefined")?!1:l,!r(o,"undefined")){var a=m(e,o);if(!r(a,"undefined"))return a}for(var f,d,c,y,v,h=["modernizr","tspan","samp"];!b.style&&h.length;)f=!0,b.modElem=p(h.shift()),b.style=b.modElem.style;for(c=e.length,d=0;c>d;d++)if(y=e[d],v=b.style[y],i(y,"-")&&(y=s(y)),b.style[y]!==t){if(l||r(o,"undefined"))return u(),"pfx"==n?y:!0;try{b.style[y]=o}catch(g){}if(b.style[y]!=v)return u(),"pfx"==n?y:!0}return u(),!1}function v(e,n,t,o,i){var s=e.charAt(0).toUpperCase()+e.slice(1),l=(e+" "+x.join(s+" ")+s).split(" ");return r(n,"string")||r(n,"undefined")?y(l,n,o,i):(l=(e+" "+_.join(s+" ")+s).split(" "),u(l,n,t))}function h(e,n,r){return v(e,t,t,n,r)}var g=[],C={_version:"3.5.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){g.push({name:e,fn:n,options:t})},addAsyncTest:function(e){g.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr;var S=[],w="Moz O ms Webkit",x=C._config.usePrefixes?w.split(" "):[];C._cssomPrefixes=x;var _=C._config.usePrefixes?w.toLowerCase().split(" "):[];C._domPrefixes=_;var z=n.documentElement,E="svg"===z.nodeName.toLowerCase(),P={elem:p("modernizr")};Modernizr._q.push(function(){delete P.elem});var b={style:P.elem.style};Modernizr._q.unshift(function(){delete b.style}),C.testAllProps=v,C.testAllProps=h,Modernizr.addTest("csstransitions",h("transition","all",!0));var T=function(n){var r,o=prefixes.length,i=e.CSSRule;if("undefined"==typeof i)return t;if(!n)return!1;if(n=n.replace(/^@/,""),r=n.replace(/-/g,"_").toUpperCase()+"_RULE",r in i)return"@"+n;for(var s=0;o>s;s++){var l=prefixes[s],u=l.toUpperCase()+"_"+r;if(u in i)return"@-"+l.toLowerCase()+"-"+n}return!1};C.atRule=T;C.prefixed=function(e,n,t){return 0===e.indexOf("@")?T(e):(-1!=e.indexOf("-")&&(e=s(e)),n?v(e,n,t):v(e,"pfx"))};o(),delete C.addTest,delete C.addAsyncTest;for(var L=0;L<Modernizr._q.length;L++)Modernizr._q[L]();e.Modernizr=Modernizr}(window,document);
    var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
        transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
        support = { transitions : Modernizr.csstransitions };

    function extend( a, b ) {
        for( var key in b ) {
            if( b.hasOwnProperty( key ) ) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function UIMorphingButton( options ) {
        this.options = extend( {}, this.options );
        extend( this.options, options );
        this._init();
    }

    UIMorphingButton.prototype.options = {
        closeEl : '',
        onBeforeOpen : function() { return false; },
        onAfterOpen : function() { return false; },
        onBeforeClose : function() { return false; },
        onAfterClose : function() { return false; }
    }

    UIMorphingButton.prototype._init = function() {
        // state
        window.mexpanded = false;

        // init events
        this._initEvents();
    }

    UIMorphingButton.prototype.open = function(target) {
        var _self = this;

        if ($('.js-modal-wrapper.open').length) {
            close('.js-modal-wrapper.open');
            open();
        } else {
            open();
        }

        function open() {
            document.querySelector(target).classList.add( 'active' );
            window.mIsAnimating = true;

            setTimeout(function () {
                window.mIsAnimating = false;
            }, 300);

            document.querySelector(target).style.top = '50%';
            document.querySelector(target).style.left = '50%';
            document.querySelector(target).style.width = '1px';
            document.querySelector(target).style.height = '1px';

            setTimeout(function () {
                document.querySelector(target).classList.add( 'open' );
                document.querySelector('.overlay').classList.add('visible');
            }, 25);

            window.mexpanded = true;
        }

        function close(el) {
            document.querySelector(el).classList.remove( 'active' );

            setTimeout(function () {
                document.querySelector(el).classList.remove( 'open' );
                document.querySelector('.overlay').classList.remove('visible');
            }, 25);
        }
    }


    UIMorphingButton.prototype._initEvents = function() {
        var self = this;
        // open
        this.options.button.addEventListener( 'click', function() { self.toggle(); } );
        // close
        if( this.options.closeEl ) {
            var closeEl = self.options.closeEl;
            if( closeEl ) {
                closeEl.addEventListener( 'click', function() { self.toggle(); } );
            }
        }
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            if( self.options.closeEl ) {
                var closeEl = self.options.closeEl;
                if( closeEl && window.mexpanded) {
                    self.toggle();
                }
            }
        };
    }

    UIMorphingButton.prototype.toggle = function() {
        if( window.mIsAnimating || ($(window).width() > 1024 && $(this.options.button).data('morph-mobile')) ) return false;

        // callback
        if( window.mexpanded ) {
            this.options.onBeforeClose();
        }
        else {
            // add class active (solves z-index problem when more than one button is in the page)
            this.options.contentEl.classList.add( 'active' );
            this.options.onBeforeOpen();
        }

        window.mIsAnimating = true;

        var self = this,
            onEndTransitionFn = function( ev ) {
                //if( ev.target !== this ) return false;

                if( support.transitions ) {
                    // open: first opacity then width/height/left/top
                    // close: first width/height/left/top then opacity
                    /*if( window.mexpanded && ev.propertyName !== 'opacity' || !window.mexpanded && ev.propertyName !== 'width' && ev.propertyName !== 'height' && ev.propertyName !== 'left' && ev.propertyName !== 'top' ) {
                        return false;
                    }*/
                    //this.removeEventListener( transEndEventName, onEndTransitionFn );
                }
                window.mIsAnimating = false;

                // callback
                if( window.mexpanded ) {
                    // remove class active (after closing)
                    self.options.contentEl.classList.remove( 'active' );
                    self.options.contentEl.classList.remove( 'close' );
                    self.options.contentEl.classList.remove( 'open' );
                    self.options.backgroundEl.classList.remove('close');
                    self.options.backgroundEl.classList.remove('visible');
                    self.options.onAfterClose();
                }
                else {
                    self.options.onAfterOpen();
                }

                window.mexpanded = !window.mexpanded;
            };

        /*if( support.transitions ) {
            this.options.contentEl.addEventListener( transEndEventName, onEndTransitionFn );
        }
        else {*/
        setTimeout(onEndTransitionFn, 300);
        

        // set the left and top values of the contentEl (same like the button)
        var buttonPos = this.options.button.getBoundingClientRect();
        // need to reset

        if( !window.mexpanded ) {
            this.options.contentEl.classList.add('no-transition');
            'left,top,width,height'.split(',').forEach(function (name) {
                self.options.contentEl.style[name] = buttonPos[name] + 'px';
            });
        }

        // add/remove class "open" to the button wraper
        setTimeout( function() {
            if( window.mexpanded ) {


                self.options.contentEl.classList.add( 'close' );
                self.options.contentEl.classList.remove( 'open' );
                setTimeout( function() {
                    'left,top,width,height'.split(',').forEach(function (name) {
                        self.options.contentEl.style[name] = buttonPos[name] + 'px';
                    });
                },1);
                self.options.backgroundEl.classList.add('close');
                self.options.backgroundEl.classList.remove('visible');
            }
            else {
                self.options.contentEl.classList.remove('no-transition');
                if(self.options.backgroundEl)
                    self.options.backgroundEl.classList.add('visible');
                setTimeout( function() {
                    self.options.contentEl.classList.add( 'open' );
                }, 25 );
            }
        }, 25 );
    }

    // add to global namespace
    window.UIMorphingButton = UIMorphingButton;
    oApp.Global.modalInit = function () {
        [].forEach.call(document.querySelectorAll('[data-morph-to]'), function(el) {
            var data = {};
            'to,close,background'.split(',').forEach(function(name) {
                var selector = el.getAttribute('data-morph-'+name);
                var items = [].slice.call(document.querySelectorAll(selector));
                if(items.length>1)
                    console.error('Too much elements ('+ items.length +') are matched "'+ name +'" selector "'+ selector+'", first one would be taken');
                data[name] = items[0];
            });

            new UIMorphingButton({
                button: el,
                contentEl: data.to,
                closeEl: data.close,
                backgroundEl: data.background
            })
        });
    };

    window.addEventListener('DOMContentLoaded', function() {
        oApp.Global.modalInit();
    });

})( window );