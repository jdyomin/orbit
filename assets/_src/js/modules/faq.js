var oApp = oApp || {};

oApp.Faq = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.faq-page')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this,
                label = $('.js-form-field');

            $('.js-question-trigger').on('click', function() {
                $('.js-question').removeClass('opened');
                $(this).closest('.js-question').toggleClass('opened');
            });

            label.on('focus', function() {
                $(this).closest('.js-field-wrapper').addClass('filled');
            });

            label.on('blur', function() {
                if (!($(this).val())) {
                    $(this).closest('.js-field-wrapper').removeClass('filled');
                }
            });
        },
        _render: function () {
            var _this = this;


        }
    };
}());