var oApp = oApp || {};

oApp.Winners = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.winners')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this;


        },
        _render: function () {
            var _this = this;

            var sliderr = new Swiper('.winners-swiper-container-1', {
                pagination: {
                    type: 'fraction',
                    el: '.swiper-pagination',
                    renderFraction: function (currentClass, totalClass) {
                        return '<span class="' + currentClass + '"></span>' +
                            ' из ' +
                            '<span class="' + totalClass + '"></span>';
                    }
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                slidesPerView: 'auto',
                loop: false,
                scrollbar: {
                    el: '.swiper-scrollbar',
                    draggable: true,
                    dragSize: 305,
                    snapOnRelease: true
                },
                observer: true
            });

            var slider2 = new Swiper('.winners-swiper-container-2', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                slidesPerView: 'auto'
            });

            var slider3 = new Swiper('.swiper-container-mob', {
                pagination: {
                    type: 'fraction',
                    el: '.swiper-pagination',
                    renderFraction: function (currentClass, totalClass) {
                        return '<span class="' + currentClass + '"></span>' +
                            ' из ' +
                            '<span class="' + totalClass + '"></span>';
                    }
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                slidesPerView: 'auto',
                centeredSlides: true
            })
        }
    };
}());