var oApp = oApp || {};

oApp.Account = library(function() {
    return {
        init: function () {
            this.elements = {
                root: $('.account')
            };

            if(!this.elements.root.length) return false;

            this._render();
            this._bind();
        },
        _bind: function () {
            var _this = this,
                tabs = $('.js-tab-control');

            tabs.on('click', function() {
                var n = $(this).index();

                $(this).closest('.js-tabs').find('.js-tab-control').removeClass('active');
                $(this).addClass('active');

                var tabs = $(this).closest('.js-tabs').find('.js-tab');
                tabs.removeClass('active');

                $.each(tabs, function() {
                    if ($(this).index() === n) {
                        $(this).addClass('active');
                    }
                })
            })
        },
        _render: function () {
            var _this = this;

            var slider = new Swiper('.nominees-swiper-container', {
                pagination: {
                    type: 'fraction',
                    el: '.swiper-pagination',
                    renderFraction: function (currentClass) {
                        return '<span class="' + currentClass + '"></span>' +
                            ' из ' +
                            '<span class="swiper-pagination-total">8</span>';
                    }
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                },
                slidesPerView: 'auto',
                breakpoints: {
                    1024: {
                        centeredSlides: true
                    }
                }
            });
        }
    };
}());