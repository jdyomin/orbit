var oApp = oApp || {};

oApp.$W = $(window);
oApp.$D = $(document);

function library(module) {
    $(function() {
        if (module.init) {
            module.init();
        }
    });
    return module;
}

jQuery.fn.removeInlineCss = (function(){
    var rootStyle = document.documentElement.style;
    var remover =
        rootStyle.removeProperty
        || rootStyle.removeAttribute;
    return function removeInlineCss(properties){
        if(properties == null)
            return this.removeAttr('style');
        proporties = properties.split(/\s+/);
        return this.each(function(){
            for(var i = 0 ; i < proporties.length ; i++)
                remover.call(this.style, proporties[i]);
        });
    };
})();